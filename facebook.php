<?php
    foreach($feed->data as $data)
    {
        if($data->type == 'status' or $data->type == 'photo' or $data->type == 'video' or $data->type == 'link'){
	        if($data->status_type == 'mobile_status_update'){
                $content .= '
                <table class="container">
                    <tr>
                        <td class="profile"><img src="http://graph.facebook.com/'.$data->from->id.'/picture?type=large" alt="'.$data->from->name.'" width="90" height="90"></td>
                        <td class="text">
                            <strong>'.$data->from->name.' update status</strong><br />
                            <p>'.$data->message.'</p>
                            <a href="'.$data->actions[0]->link.'">View on Facebook</a>
                        </td>
                    </tr>
                </table>
                <div class="clean"></div>
                ';
            }
            elseif($data->status_type == 'added_photos'){
                $content .= '
                <table class="container">
                    <tr>
                        <td class="profile"><img src="http://graph.facebook.com/'.$data->from->id.'/picture?type=large" alt="'.$data->from->name.'" width="90" height="90"></td>
                        <td class="text">
                            <strong>'.$data->from->name.' added a picture</strong><br />
                            <p>'.$data->message.'</p>
                            <p><img src="'.$data->picture.'"></p>
                            <a href="'.$data->actions[0]->link.'">View on Facebook</a>
                        </td>
                    </tr>
                </table>
                <div class="clean"></div>
                ';
            }
            elseif($data->status_type == 'shared_story'){
                if($data->type == "link")
                {
                    $content .= '
                    <table class="container">
                        <tr>
                            <td class="profile"><img src="http://graph.facebook.com/'.$data->from->id.'/picture?type=large" alt="'.$data->from->name.'" width="90" height="90"></td>
                            <td class="text">
                                <strong>'.$data->from->name.' shared a link</strong><br />
                                <p>'.$data->message.'</p>
                                <table class="link">
                                    <tr>
                                        <td valign="top"><a href="'.$data->link.'"><img src="'.$data->picture.'"></a></td>
                                        <td>
                                            <p>'.$data->name.'</p>
                                            <p>'.$data->description.'</p>
                                        </td>
                                    </tr>
                                </table>
                                <a href="'.$data->actions[0]->link.'">View on Facebook</a>
                            </td>
                        </tr>
                    </table>
                    <div class="clean"></div>
                    ';   
                }
                if($data->type == "video")
                {
                    $content .= '
                    <table class="container">
                        <tr>
                            <td class="profile"><img src="http://graph.facebook.com/'.$data->from->id.'/picture?type=large" alt="'.$data->from->name.'" width="90" height="90"></td>
                            <td class="text">
                                <strong>'.$data->from->name.' shared a video</strong><br />
                                <p>'.$data->message.'</p>
                                <table class="link">
                                    <tr>
                                        <td valign="top"><a href="'.$data->link.'"><img src="'.$data->picture.'"></a></td>
                                        <td>
                                            <p>'.$data->name.'</p>
                                            <p>'.$data->description.'</p>
                                        </td>
                                    </tr>
                                </table>
                                <a href="'.$data->actions[0]->link.'">View on Facebook</a>
                            </td>
                        </tr>
                    </table>
                    <div class="clean"></div>
                    ';   
                }
            }
        }
    }
?>